# Tor overlay network base

This repository is a fork of [https://gitlab.com/fredfall/rtun](https://gitlab.com/fredfall/rtun) with some refactoring and documentation.

## Usage

```
usage: rtun/rtun.py [-h] [-r RELAY] [-g GUARD] [-l] [-c] [-k COOKIE] [-p] [-K] [-t TUNNEL_NAME] [-n NAMESPACE] [-i ID] [-d DID] [-R] [-x]

optional arguments:
  -h, --help            show this help message and exit
  -r RELAY, --relay RELAY
                        relay to be used as rendezvous point
  -g GUARD, --guard GUARD
                        optional router to be used as guard node
  -l, --listen          create the rendezvous point an wait
  -c, --connect         connect to an already established rendezvous point
  -k COOKIE, --cookie COOKIE
                        a 20 byte rendezvous cookie
  -p, --pairwise        select relay automatically using the pairwise algorithm
  -t TUNNEL_NAME, --tunnel_name TUNNEL_NAME
                        name of the tunnel, default is the two peers name concatenated
  -n NAMESPACE, --namespace NAMESPACE
                        secret key used to differentiate between different nets
  -i ID, --id ID        id of our own client(used for addressing and port allocation)
  -d DID, --did DID     destination id
  -R, --renew           renew Tor directory server list
  -x, --dummy           do not connect, only test
```

## Installation guide

### Prerequisites

- openVPN
- Python3
- Pip3
- git

### Virtualenv

Create a virtual environment for Python:

```bash
pip3 install virtualenv
python3 -m virtualenv -p python3 venv
source venv/bin/activate
```

This creates a new folder `venv/`, with a virtual python environment and activates it. This makes it so you can keep the required packages seperate from other projects. Write `deactivate` to exit the virtual environment.

### Installing dependencies

While inside of the virtual environment.

```bash
pip install -r requirements.txt -r requirements-dev.txt
```

This install all the dependencies and puts them in the `venv/` folder

**NB** We use our own fork of the package `torpy`. This should be downloaded automaticly with the command above.
