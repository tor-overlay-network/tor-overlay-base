#!/usr/bin/env python
import argparse

from bootstrapping.relay_choosers import choose_relay_from_tunnel
from rendezvous import two_hop
from server import list_rend_server

import subprocess
from time import sleep
from torpy.consesus import TorConsensus


import logging

logging.basicConfig(level=logging.DEBUG)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r", "--relay", help="relay to be used as rendezvous point", type=str
    )
    parser.add_argument(
        "-g", "--guard", help="optional router to be used as guard node", type=str
    )
    parser.add_argument(
        "-l",
        "--listen",
        action="store_true",
        help="create the rendezvous point an wait",
    )
    parser.add_argument(
        "-c",
        "--connect",
        action="store_true",
        help="connect to an already established rendezvous point",
    )
    parser.add_argument("-k", "--cookie", help="a 20 byte rendezvous cookie", type=str)
    parser.add_argument(
        "-p",
        "--pairwise",
        action="store_true",
        help="select relay automatically using the pairwise algorithm",
    )

    parser.add_argument(
        "-t",
        "--tunnel_name",
        help="name of the tunnel, default is the two peers" " name concatenated",
        type=str,
    )
    parser.add_argument(
        "-n",
        "--namespace",
        help="secret key used to differentiate between different nets",
        type=str,
    )
    parser.add_argument(
        "-i",
        "--id",
        help="id of our own client(used for addressing and port allocation)",
        type=int,
    )
    parser.add_argument("-d", "--did", help="destination id", type=int)

    parser.add_argument(
        "-R",
        "--renew",
        action="store_true",
        help="renew Tor directory server list",
    )
    parser.add_argument(
        "-x", "--dummy", action="store_true", help="do not connect, only test"
    )
    return parser.parse_args()


def main():
    args = parse_args()

    if args.connect and args.listen:
        print(f"Illegal combination of arguments")
        exit()

    if args.namespace:
        namespace = args.namespace
    else:
        namespace = "default"

    if args.relay:
        relay_nick = args.relay

    if args.pairwise:
        relay_nick, cookie = choose_relay_from_tunnel(
            args.tunnel_name, namespace=namespace
        )

    if args.guard:
        guard_nick = args.guard
    else:
        guard_nick = "XXX"

    if not args.cookie:
        cookie = "00000000000000000001".encode("UTF-8")
    else:
        if len(args.cookie) != 20:
            print("Cookie is of unacceptable length")
            exit()
        cookie = args.cookie.encode("UTF-8")

    if args.dummy:
        exit()

    if args.renew:
        consensus = TorConsensus()
        consensus.renew()

    if args.listen:
        print(relay_nick)
        openvpn_client = subprocess.Popen(
            [
                "/usr/sbin/openvpn",
                "--remote",
                "127.0.0.1",
                "--proto",
                "tcp-client",
                "--secret",
                "static.key",
                "--socks-proxy",
                "127.0.0.1",
                f"105{args.did}",
                "--ifconfig",
                f"10.{args.id}.0.1",
                f"10.{args.did}.0.1",
                "--dev",
                f"tun{args.did}",
                "--port",
                f"119{args.did}",
            ]
        )

        two_hop(cookie, relay_nick, guard_nick, int("105" + str(args.did)))

        openvpn_client.terminate()

    if args.connect:
        print(relay_nick)
        print("Starting openvpn server")
        openvpn_server = subprocess.Popen(
            [
                "/usr/sbin/openvpn",
                "--proto",
                "tcp-server",
                "--secret",
                "static.key",
                "--ifconfig",
                f"10.{args.id}.0.1",
                f"10.{args.did}.0.1",
                "--dev",
                f"tun{args.did}",
                "--port",
                f"119{args.id}",
            ]
        )
        sleep(1)
        try:
            list_rend_server(cookie, relay_nick)
        except BaseException:
            print("Error, terminating")
        openvpn_server.terminate()


if __name__ == "__main__":
    main()
