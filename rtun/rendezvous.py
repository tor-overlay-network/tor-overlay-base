import os
from functools import partial
import logging
from torpy.cell_socket import TorCellSocket
from torpy.cells import (
    CellRelayEstablishRendezvous,
    CellCreate2,
    CellCreated2,
    CellRelay,
    CellCreateFast,
    CellCreatedFast,
    CellRelayRendezvous2,
    CellRelayRendezvous1,
)
from torpy.circuit import CircuitNode
from torpy.keyagreement import NtorKeyAgreement, FastKeyAgreement

from torpy.cell_socket import TorCellSocket

from torpy.consesus import TorConsensus

from torpy import TorClient
from torpy.cli.socks import SocksServer
from torpy.crypto_state import CryptoState

import logging

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO)


def two_hop(cookie, router_nick, guard_nick, port_num):
    with TorClient() as tor:

        with tor.get_guard(nick=guard_nick) as guard:
            circuit = guard._circuits.create_new()
            try:
                circuit.create()

                router = circuit._guard.consensus.get_router_using_nick(router_nick)
                circuit.extend(router)
                circuit._rendezvous_establish(cookie)
                logger.info("Circuit has been built")

                with circuit.create_waiter(CellRelayRendezvous2) as w:
                    rendezvous2_cell = w.get(timeout=200)
                    logger.info("Got REND2 message")

                extend_node = CircuitNode(router, key_agreement_cls=FastKeyAgreement)
                shared_sec = "000000000000000000010000000000000000000100000000000000010000000000000001".encode(
                    "utf-8"
                )

                extend_node._crypto_state = CryptoState(shared_sec)

                circuit._circuit_nodes.append(extend_node)

                with SocksServer(circuit, "127.0.0.1", port_num) as socks_serv:
                    socks_serv.start()

            except Exception:
                # We must close here because we didn't enter to circuit yet to guard by context manager
                circuit.close()
                raise
            circuit.close()


def set_up_rendezvous_point(nick, cookie):

    consensus = TorConsensus()
    router = consensus.get_router_using_nick(nick)
    tor_cell_socket = TorCellSocket(router)
    tor_cell_socket.connect()

    circuit_id = 0x80000001

    if False:
        key_agreement_cls = NtorKeyAgreement
        create_cls = partial(CellCreate2, key_agreement_cls.TYPE)
        created_cls = CellCreated2
    else:
        key_agreement_cls = FastKeyAgreement
        create_cls = CellCreateFast
        created_cls = CellCreatedFast

    circuit_node = CircuitNode(router, key_agreement_cls=key_agreement_cls)
    onion_skin = circuit_node.create_onion_skin()

    cell_create = create_cls(onion_skin, circuit_id)

    tor_cell_socket.send_cell(cell_create)
    cell_created = tor_cell_socket.recv_cell()

    logger.debug("Verifying response...")
    circuit_node.complete_handshake(cell_created.handshake_data)

    logger.debug(cell_created.circuit_id)

    circuit_node.complete_handshake(cell_created.handshake_data)

    rendezvous_cookie = cookie

    inner_cell = CellRelayEstablishRendezvous(
        rendezvous_cookie, cell_created.circuit_id
    )

    relay_cell = CellRelay(inner_cell, stream_id=0, circuit_id=circuit_id)

    circuit_node.encrypt_forward(relay_cell)

    tor_cell_socket.send_cell(relay_cell)
    rcv_cell = tor_cell_socket.recv_cell()
    circuit_node.decrypt_backward(rcv_cell)

    logger.debug(rcv_cell)

    return tor_cell_socket, circuit_node


def connect_to_rendezvous_point(nick, cookie):

    consensus = TorConsensus()
    router = consensus.get_router_using_nick(nick)

    tor_cell_socket = TorCellSocket(router)
    tor_cell_socket.connect()

    circuit_id = 0x80000002

    if False:
        key_agreement_cls = NtorKeyAgreement
        create_cls = partial(CellCreate2, key_agreement_cls.TYPE)
        created_cls = CellCreated2
    else:
        key_agreement_cls = FastKeyAgreement
        create_cls = CellCreateFast
        created_cls = CellCreatedFast

    circuit_node = CircuitNode(router, key_agreement_cls=key_agreement_cls)
    onion_skin = circuit_node.create_onion_skin()
    cell_create = create_cls(onion_skin, circuit_id)
    tor_cell_socket.send_cell(cell_create)
    cell_created = tor_cell_socket.recv_cell()
    logger.debug("Verifying response...")
    circuit_node.complete_handshake(cell_created.handshake_data)

    logger.debug(cell_created.circuit_id)

    circuit_node.complete_handshake(cell_created.handshake_data)

    inner_cell = CellRelayRendezvous1(
        os.urandom(128 + 20), cookie, cell_created.circuit_id
    )

    relay_cell = CellRelay(inner_cell, stream_id=0, circuit_id=circuit_id)

    circuit_node.encrypt_forward(relay_cell)

    tor_cell_socket.send_cell(relay_cell)

    return tor_cell_socket, circuit_node, circuit_id
