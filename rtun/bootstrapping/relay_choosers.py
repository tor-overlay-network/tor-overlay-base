from __future__ import annotations
from rtun.bootstrapping.utils import (
    getRotatedTimeStamp,
    generate_hash_from,
    get_relay_index_from_hash,
    get_cookie_from_hash,
    get_all_relays,
)

import logging


def choose_relay_from_tunnel(
    tun_name: str, namespace="default", time_frame: str = "1 min"
) -> tuple([]):
    logging.info("Choosing relay")

    all_relays = get_all_relays()
    num_of_relays = len(all_relays)
    logging.info(f"Number of relays: {num_of_relays}")

    time_stamp = getRotatedTimeStamp(time_frame)
    h = generate_hash_from(time_stamp, namespace, tun_name)

    selected_relay_index = get_relay_index_from_hash(h, num_of_relays)
    selected_relay = all_relays[selected_relay_index]
    cookie = get_cookie_from_hash(h)

    logging.info(
        f"I want to connect to {selected_relay['name']} at {selected_relay['address']}:{selected_relay['port']} with cookie {cookie}"
    )

    return selected_relay["name"], cookie
