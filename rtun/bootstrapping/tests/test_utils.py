import pytest
from rtun.bootstrapping.utils import (
    generate_hash_from,
    get_relay_index_from_hash,
)


def test_get_relay_index_from_hash():
    hash = generate_hash_from("this is a string that will be hashed")
    with pytest.raises(ZeroDivisionError, match=r"integer division or modulo by zero"):
        get_relay_index_from_hash(hash, 0) == 0

    assert get_relay_index_from_hash(hash, 1) == 0
    assert get_relay_index_from_hash(hash, 2) == 1
