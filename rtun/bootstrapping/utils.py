from __future__ import annotations

import pandas as pd
import hashlib

import logging

def getRotatedTimeStamp(time_frame="1 min"):
    # How long to use 1 relay as RP. Rotates every :30 seconds with the round-function in Pandas when timeframe="1 min".
    return pd.Timestamp.now().round(time_frame).value


def generate_hash_from(*args):
    builder = b""
    for arg in args:
        if type(arg) == bytes:
            builder += arg
        else:
            builder += str(arg).encode()
    return hashlib.sha256(builder)


def get_relay_index_from_hash(hash: hashlib._Hash, num_of_relays: int) -> int:
    n = int(hash.hexdigest(), base=16)  # Convert to integer to be able to use modulo
    return n % num_of_relays


def get_cookie_from_hash(hash: hashlib._Hash) -> bytes:
    return hash.hexdigest().encode()[0:20]


def get_all_relays() -> list[dict[str, str]]:
    """
    Reads all relays from file and returns them as a list of lists.
    Each relay is a list that contains [name, address, port].
    """

    all_relays = []
    a_r = []
    blacklist = []
    actual_relays = []
    logging.info(f"Reads relays from file: /root/.local/share/torpy/network_status")
    with open("/root/.local/share/torpy/network_status", "r") as f:
        line = True
        while line:
            line = f.readline()
            data = line.split(" ")
            data_type = data[0]
            if data_type == "r":  # relay
                name = data[1]
                if name in a_r:
                    blacklist.append(name)
                a_r.append(name)
                [address, port] = data[6:8]
                all_relays.append({"name": name, "address": address, "port": port})
    logging.info(
        f"Done reading relays from file: /root/.local/share/torpy/network_status\n"
        f"Found {len(all_relays)} relays\n"
        f"Blacklisted {len(blacklist)} relays\n"
        f"Actual relays {len(actual_relays)}\n"
    )
    for rel in all_relays:
        if not rel["name"] in blacklist:
            actual_relays.append(rel)
    return actual_relays